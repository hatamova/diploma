'use strict';

var gulp = require('gulp'), //основной плагин gulp
    sass = require('gulp-sass'), //препроцессор sass
  	
    sourcemaps = require('gulp-sourcemaps'), //sourcemaps
    watch = require('gulp-watch'), //расширение возможностей watch
    connect = require('gulp-connect'); //livereload


var path = {
    build: { //Тут мы укажем куда складывать готовые после сборки файлы
        html: 'build/',
        js: 'build/js/',
      	css: 'build/css/',
        img: 'build/img/'
    },

    src: { //Пути откуда брать исходники
        html: 'src/*.html', 
        js: 'src/js/[^_]*.js',
       
        sass: 'src/styles/**/*.scss',
      	img: 'src/img/**/*.*', 
      
    },
    watch: { //Тут мы укажем, за изменением каких файлов мы хотим наблюдать
        html: 'src/*.html',
        js: 'src/js/**/*.js',
        sass: 'src/styles/**/*.scss',
        img: 'src/css/images/**/*.*',
        fonts: 'src/fonts/**/*.*',
    },

    clean: './build', 
    outputDir: './build' 
};

// Локальный сервер для разработки
gulp.task('connect', function(){
    connect.server({ 
        root: [path.outputDir],
        port: 9999, 
        livereload: true 
    });
});

// таск для билдинга html
gulp.task('html:build', function () {
    gulp.src(path.src.html) 
        .pipe(gulp.dest(path.build.html))
        .pipe(connect.reload()) 
});



// билдинг яваскрипта
gulp.task('js:build', function () {
    gulp.src(path.src.js)
        .pipe(sourcemaps.init())
        .pipe(sourcemaps.write()) 
        .pipe(gulp.dest(path.build.js)) 
        .pipe(connect.reload()) 
});


// билдим статичные изображения
gulp.task('image:build', function () {
    gulp.src(path.src.img) 
        .pipe(gulp.dest(path.build.img)) 
        .pipe(connect.reload()) 


// билдинг домашнего css
gulp.task('sass:build', function () {
    gulp.src(path.src.sass) 
        .pipe(sourcemaps.init()) 
        .pipe(sass().on('error', sass.logError)) 
        .pipe(sourcemaps.write()) 
        .pipe(gulp.dest(path.build.css)) 
        .pipe(connect.reload()) 
});


// билдим все
gulp.task('build', [
    'html:build',
   
    'js:build',
    'sass:build',
    'image:build',
]);



// watch
gulp.task('watch', function(){
     //билдим html в случае изменения
    watch([path.watch.html], function(event, cb) {
        gulp.start('html:build');
    });
     
     //билдим css в случае изменения
    watch([path.watch.sass], function(event, cb) {
        gulp.start('sass:build');
    });

  
     //билдим js в случае изменения
    watch([path.watch.js], function(event, cb) {
        gulp.start('js:build');
    });
     //билдим статичные изображения в случае изменения
    watch([path.watch.img], function(event, cb) {
        gulp.start('image:build');
    });
   
    
});

// действия по умолчанию
gulp.task('default', ['build', 'watch', 'connect']);