var bookApp = angular.module('bookApp', ["ngSanitize"]);

/* Вывод всех книг на главную страницу*/
bookApp.controller('booksController', function($scope, $http) {
  $scope.limitBook = 4;

	$http.get('https://netology-fbb-store-api.herokuapp.com/book').success(function(data) {
    	$scope.books = data;
      $scope.countBook = $scope.books.length;
  	});

  $scope.showButton = function(limitBook) { /* отображение книг по 4 шт*/
    console.log(limitBook);
    if (limitBook >= $scope.countBook) {
      return false;
    } else {
      return true;
    }
  }

	$scope.goToBook = function (bookId) { /* переход на страницу покупки книги*/
  		location.href = "book.html?"+ bookId;
  	};
});

/* Вывод данных о книге по id */
bookApp.controller('bookController', function($scope, $http) {
	var id = document.location.href.split ('?') [1];
  	$http.get('https://netology-fbb-store-api.herokuapp.com/book/' + id).success(function(data) {
	    $scope.book = data;
	});

  $scope.buyBook = function (user, bookId, buyForm) { /* отправка данных формы для заказа */
   
    if(buyForm.$invalid) {
        $scope.errorForm = {visible : true}; /* проверка валидности формы */
      } else {
          var body = {
            manager: "Nikky3810@yandex.ru",
            book: bookId,
            name: user.name,
            phone: user.phone,
            email: user.email,
            comment: user.comment,
            delivery:
              {id: user.delivery.id,
              address: user.address},
            payment:
              {id: user.payment.id,
              currency: user.delivery.currency}
          };
      
          $http.post('https://netology-fbb-store-api.herokuapp.com/order', body).success(function(data, status){
            $scope.form = {visible : false}; /*скрываем форму*/
            $scope.error = false;
            $scope.success = true;
          }).error(function(data, status){
            $scope.success = false;
            $scope.error = true;
          });
      }
  };

}); 		
 
/* Получение данных о способах доставки и способах оплаты */ 	
bookApp.controller('deliveriesController', function($scope, $http) {
    $http.get('https://netology-fbb-store-api.herokuapp.com/order/delivery').success(function(data) {
      $scope.deliveries = data;
    });

    $scope.deliveryAndPay = function(delivery) {
      $http.get('https://netology-fbb-store-api.herokuapp.com/order/delivery/' + delivery.id +'/payment').success(function(data) {
        $scope.payment = data;
      });
      
      $scope.pay = {visible : true};

      $scope.deliveryPrice = delivery.price;
      $scope.buy = {visible : true};
    };
});





	






//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlcyI6WyJtYWluLmpzIl0sInNvdXJjZXNDb250ZW50IjpbInZhciBib29rQXBwID0gYW5ndWxhci5tb2R1bGUoJ2Jvb2tBcHAnLCBbXCJuZ1Nhbml0aXplXCJdKTtcclxuXHJcbi8qINCS0YvQstC+0LQg0LLRgdC10YUg0LrQvdC40LMg0L3QsCDQs9C70LDQstC90YPRjiDRgdGC0YDQsNC90LjRhtGDKi9cclxuYm9va0FwcC5jb250cm9sbGVyKCdib29rc0NvbnRyb2xsZXInLCBmdW5jdGlvbigkc2NvcGUsICRodHRwKSB7XHJcbiAgJHNjb3BlLmxpbWl0Qm9vayA9IDQ7XHJcblxyXG5cdCRodHRwLmdldCgnaHR0cHM6Ly9uZXRvbG9neS1mYmItc3RvcmUtYXBpLmhlcm9rdWFwcC5jb20vYm9vaycpLnN1Y2Nlc3MoZnVuY3Rpb24oZGF0YSkge1xyXG4gICAgXHQkc2NvcGUuYm9va3MgPSBkYXRhO1xyXG4gICAgICAkc2NvcGUuY291bnRCb29rID0gJHNjb3BlLmJvb2tzLmxlbmd0aDtcclxuICBcdH0pO1xyXG5cclxuICAkc2NvcGUuc2hvd0J1dHRvbiA9IGZ1bmN0aW9uKGxpbWl0Qm9vaykgeyAvKiDQvtGC0L7QsdGA0LDQttC10L3QuNC1INC60L3QuNCzINC/0L4gNCDRiNGCKi9cclxuICAgIGNvbnNvbGUubG9nKGxpbWl0Qm9vayk7XHJcbiAgICBpZiAobGltaXRCb29rID49ICRzY29wZS5jb3VudEJvb2spIHtcclxuICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuXHQkc2NvcGUuZ29Ub0Jvb2sgPSBmdW5jdGlvbiAoYm9va0lkKSB7IC8qINC/0LXRgNC10YXQvtC0INC90LAg0YHRgtGA0LDQvdC40YbRgyDQv9C+0LrRg9C/0LrQuCDQutC90LjQs9C4Ki9cclxuICBcdFx0bG9jYXRpb24uaHJlZiA9IFwiYm9vay5odG1sP1wiKyBib29rSWQ7XHJcbiAgXHR9O1xyXG59KTtcclxuXHJcbi8qINCS0YvQstC+0LQg0LTQsNC90L3Ri9GFINC+INC60L3QuNCz0LUg0L/QviBpZCAqL1xyXG5ib29rQXBwLmNvbnRyb2xsZXIoJ2Jvb2tDb250cm9sbGVyJywgZnVuY3Rpb24oJHNjb3BlLCAkaHR0cCkge1xyXG5cdHZhciBpZCA9IGRvY3VtZW50LmxvY2F0aW9uLmhyZWYuc3BsaXQgKCc/JykgWzFdO1xyXG4gIFx0JGh0dHAuZ2V0KCdodHRwczovL25ldG9sb2d5LWZiYi1zdG9yZS1hcGkuaGVyb2t1YXBwLmNvbS9ib29rLycgKyBpZCkuc3VjY2VzcyhmdW5jdGlvbihkYXRhKSB7XHJcblx0ICAgICRzY29wZS5ib29rID0gZGF0YTtcclxuXHR9KTtcclxuXHJcbiAgJHNjb3BlLmJ1eUJvb2sgPSBmdW5jdGlvbiAodXNlciwgYm9va0lkLCBidXlGb3JtKSB7IC8qINC+0YLQv9GA0LDQstC60LAg0LTQsNC90L3Ri9GFINGE0L7RgNC80Ysg0LTQu9GPINC30LDQutCw0LfQsCAqL1xyXG4gICBcclxuICAgIGlmKGJ1eUZvcm0uJGludmFsaWQpIHtcclxuICAgICAgICAkc2NvcGUuZXJyb3JGb3JtID0ge3Zpc2libGUgOiB0cnVlfTsgLyog0L/RgNC+0LLQtdGA0LrQsCDQstCw0LvQuNC00L3QvtGB0YLQuCDRhNC+0YDQvNGLICovXHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICB2YXIgYm9keSA9IHtcclxuICAgICAgICAgICAgbWFuYWdlcjogXCJOaWtreTM4MTBAeWFuZGV4LnJ1XCIsXHJcbiAgICAgICAgICAgIGJvb2s6IGJvb2tJZCxcclxuICAgICAgICAgICAgbmFtZTogdXNlci5uYW1lLFxyXG4gICAgICAgICAgICBwaG9uZTogdXNlci5waG9uZSxcclxuICAgICAgICAgICAgZW1haWw6IHVzZXIuZW1haWwsXHJcbiAgICAgICAgICAgIGNvbW1lbnQ6IHVzZXIuY29tbWVudCxcclxuICAgICAgICAgICAgZGVsaXZlcnk6XHJcbiAgICAgICAgICAgICAge2lkOiB1c2VyLmRlbGl2ZXJ5LmlkLFxyXG4gICAgICAgICAgICAgIGFkZHJlc3M6IHVzZXIuYWRkcmVzc30sXHJcbiAgICAgICAgICAgIHBheW1lbnQ6XHJcbiAgICAgICAgICAgICAge2lkOiB1c2VyLnBheW1lbnQuaWQsXHJcbiAgICAgICAgICAgICAgY3VycmVuY3k6IHVzZXIuZGVsaXZlcnkuY3VycmVuY3l9XHJcbiAgICAgICAgICB9O1xyXG4gICAgICBcclxuICAgICAgICAgICRodHRwLnBvc3QoJ2h0dHBzOi8vbmV0b2xvZ3ktZmJiLXN0b3JlLWFwaS5oZXJva3VhcHAuY29tL29yZGVyJywgYm9keSkuc3VjY2VzcyhmdW5jdGlvbihkYXRhLCBzdGF0dXMpe1xyXG4gICAgICAgICAgICAkc2NvcGUuZm9ybSA9IHt2aXNpYmxlIDogZmFsc2V9OyAvKtGB0LrRgNGL0LLQsNC10Lwg0YTQvtGA0LzRgyovXHJcbiAgICAgICAgICAgICRzY29wZS5lcnJvciA9IGZhbHNlO1xyXG4gICAgICAgICAgICAkc2NvcGUuc3VjY2VzcyA9IHRydWU7XHJcbiAgICAgICAgICB9KS5lcnJvcihmdW5jdGlvbihkYXRhLCBzdGF0dXMpe1xyXG4gICAgICAgICAgICAkc2NvcGUuc3VjY2VzcyA9IGZhbHNlO1xyXG4gICAgICAgICAgICAkc2NvcGUuZXJyb3IgPSB0cnVlO1xyXG4gICAgICAgICAgfSk7XHJcbiAgICAgIH1cclxuICB9O1xyXG5cclxufSk7IFx0XHRcclxuIFxyXG4vKiDQn9C+0LvRg9GH0LXQvdC40LUg0LTQsNC90L3Ri9GFINC+INGB0L/QvtGB0L7QsdCw0YUg0LTQvtGB0YLQsNCy0LrQuCDQuCDRgdC/0L7RgdC+0LHQsNGFINC+0L/Qu9Cw0YLRiyAqLyBcdFxyXG5ib29rQXBwLmNvbnRyb2xsZXIoJ2RlbGl2ZXJpZXNDb250cm9sbGVyJywgZnVuY3Rpb24oJHNjb3BlLCAkaHR0cCkge1xyXG4gICAgJGh0dHAuZ2V0KCdodHRwczovL25ldG9sb2d5LWZiYi1zdG9yZS1hcGkuaGVyb2t1YXBwLmNvbS9vcmRlci9kZWxpdmVyeScpLnN1Y2Nlc3MoZnVuY3Rpb24oZGF0YSkge1xyXG4gICAgICAkc2NvcGUuZGVsaXZlcmllcyA9IGRhdGE7XHJcbiAgICB9KTtcclxuXHJcbiAgICAkc2NvcGUuZGVsaXZlcnlBbmRQYXkgPSBmdW5jdGlvbihkZWxpdmVyeSkge1xyXG4gICAgICAkaHR0cC5nZXQoJ2h0dHBzOi8vbmV0b2xvZ3ktZmJiLXN0b3JlLWFwaS5oZXJva3VhcHAuY29tL29yZGVyL2RlbGl2ZXJ5LycgKyBkZWxpdmVyeS5pZCArJy9wYXltZW50Jykuc3VjY2VzcyhmdW5jdGlvbihkYXRhKSB7XHJcbiAgICAgICAgJHNjb3BlLnBheW1lbnQgPSBkYXRhO1xyXG4gICAgICB9KTtcclxuICAgICAgXHJcbiAgICAgICRzY29wZS5wYXkgPSB7dmlzaWJsZSA6IHRydWV9O1xyXG5cclxuICAgICAgJHNjb3BlLmRlbGl2ZXJ5UHJpY2UgPSBkZWxpdmVyeS5wcmljZTtcclxuICAgICAgJHNjb3BlLmJ1eSA9IHt2aXNpYmxlIDogdHJ1ZX07XHJcbiAgICB9O1xyXG59KTtcclxuXHJcblxyXG5cclxuXHJcblxyXG5cdFxyXG5cclxuXHJcblxyXG5cclxuXHJcbiJdLCJmaWxlIjoibWFpbi5qcyJ9
