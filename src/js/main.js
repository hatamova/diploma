var bookApp = angular.module('bookApp', ["ngSanitize"]);

/* Вывод всех книг на главную страницу*/
bookApp.controller('booksController', function($scope, $http) {
  $scope.limitBook = 4;

	$http.get('https://netology-fbb-store-api.herokuapp.com/book').success(function(data) {
    	$scope.books = data;
      $scope.countBook = $scope.books.length;
  	});

  $scope.showButton = function(limitBook) { /* отображение книг по 4 шт*/
    console.log(limitBook);
    if (limitBook >= $scope.countBook) {
      return false;
    } else {
      return true;
    }
  }

	$scope.goToBook = function (bookId) { /* переход на страницу покупки книги*/
  		location.href = "book.html?"+ bookId;
  	};
});

/* Вывод данных о книге по id */
bookApp.controller('bookController', function($scope, $http) {
	var id = document.location.href.split ('?') [1];
  	$http.get('https://netology-fbb-store-api.herokuapp.com/book/' + id).success(function(data) {
	    $scope.book = data;
	});

  $scope.buyBook = function (user, bookId, buyForm) { /* отправка данных формы для заказа */
   
    if(buyForm.$invalid) {
        $scope.errorForm = {visible : true}; /* проверка валидности формы */
      } else {
          var body = {
            manager: "Nikky3810@yandex.ru",
            book: bookId,
            name: user.name,
            phone: user.phone,
            email: user.email,
            comment: user.comment,
            delivery:
              {id: user.delivery.id,
              address: user.address},
            payment:
              {id: user.payment.id,
              currency: user.delivery.currency}
          };
      
          $http.post('https://netology-fbb-store-api.herokuapp.com/order', body).success(function(data, status){
            $scope.form = {visible : false}; /*скрываем форму*/
            $scope.error = false;
            $scope.success = true;
          }).error(function(data, status){
            $scope.success = false;
            $scope.error = true;
          });
      }
  };

}); 		
 
/* Получение данных о способах доставки и способах оплаты */ 	
bookApp.controller('deliveriesController', function($scope, $http) {
    $http.get('https://netology-fbb-store-api.herokuapp.com/order/delivery').success(function(data) {
      $scope.deliveries = data;
    });

    $scope.deliveryAndPay = function(delivery) {
      $http.get('https://netology-fbb-store-api.herokuapp.com/order/delivery/' + delivery.id +'/payment').success(function(data) {
        $scope.payment = data;
      });
      
      $scope.pay = {visible : true};

      $scope.deliveryPrice = delivery.price;
      $scope.buy = {visible : true};
    };
});





	





